# assignment

Clone the project Rabobank (Spring BootT project).

`git clone https://gitlab.com/m89alireza/assignment.git`

Run maven command to install dependency.

This application have one active service. 

please put the address of the input `csv/xml` file into the `VM arguments` part of the `Run Configuration`
like `-DfilePath={file path}`

and then run the application

after run the application the result will be written in the log file in the current path of the project with name `result.log`
