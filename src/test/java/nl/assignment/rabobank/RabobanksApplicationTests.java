package nl.assignment.rabobank;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.UnmarshalException;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import nl.assignment.rabobank.factory.CsvFileExtractor;
import nl.assignment.rabobank.factory.Extractor;
import nl.assignment.rabobank.factory.FileExtractorFactory;
import nl.assignment.rabobank.factory.XmlFileExtractor;
import nl.assignment.rabobank.model.Record;
import nl.assignment.rabobank.service.ValidatorServiceImpl;


class RabobanksApplicationTests {
	

	private final File inputCsvFile = new File(getClass().getClassLoader().getResource("records.csv").getFile());
	private final File inputXmlFile = new File(getClass().getClassLoader().getResource("records.xml").getFile());
	private final File inputXmlFileUnmarshal = new File(getClass().getClassLoader().getResource("recordsTest.xml").getFile());
	private final Extractor csvFileExtractor = new CsvFileExtractor(inputCsvFile);
	private final Extractor xmlFileExtractor = new XmlFileExtractor(inputXmlFile);
	private static final FileExtractorFactory fileExtractorFactory = new FileExtractorFactory();
	private static final ValidatorServiceImpl validatorService = new ValidatorServiceImpl();

	/**
	 * 
	 * scenario : Processing the input CSV file and extracting
	 * values for process
	 */
	@Test
	public void extractStatmentFromCSVTest() throws Exception {
		
		int totalLineInInputCsv = 10; // input CSV file has 10 records.
		List<Record> extractedRecords = csvFileExtractor.fileExtractor();
		assertEquals(totalLineInInputCsv, extractedRecords.size());
	}
	/**
	 *  
	 * scenario : Processing the input XML file and extracting
	 * values for process
	 */
	@Test
	public void extractStatmentFromXMLTest() throws Exception {
		
		int totalLineInInputXML = 10; // input XML file has 10 records.
		List<Record> extractedRecords = xmlFileExtractor.fileExtractor();
		assertEquals(totalLineInInputXML, extractedRecords.size());
	}
	
	/**
	 *  
	 * scenario : give a XML file to FactoryMethod and it should return right instance
	 * Extrctor
	 */
	@Test
	public void extractFactoryMethodXmlTest() throws Exception {
		Extractor extractor=fileExtractorFactory.extractor(inputXmlFile);
		assertTrue(extractor instanceof XmlFileExtractor);
	}
	
	/**
	 *  
	 * scenario : give a CSV file to FactoryMethod and it should return right instance
	 * Extrctor
	 */
	@Test
	public void extractFactoryMethodCsvTest() throws Exception {
		Extractor extractor=fileExtractorFactory.extractor(inputCsvFile);
		assertTrue(extractor instanceof CsvFileExtractor);
	}
	
	/**
	 *  
	 * scenario : UnmarshalException in Extractor
	 * 
	 */
	@Test
	public void getUnmarshalExceptionTest() throws Exception{
		Extractor extractor=fileExtractorFactory.extractor(inputXmlFileUnmarshal);
		assertThrows(UnmarshalException.class,() -> extractor.fileExtractor());
	
	}
	/**
	 *  
	 * scenario : fileNotFoundException in Extractor
	 * 
	 */

	@Test
	public void getFileNotFoundExceptionTest() throws Exception{
		File inputFile = new File("dddd");
		assertThrows(FileNotFoundException.class,() -> fileExtractorFactory.extractor(inputFile));
	
	}
	
	/**
	 * 
	 * scenario : Duplicate check in given statements
	 * 
	 */
	@Test
	public void getDuplicateRecordsWithWrongValueTest() {
		List<Record> inputList = Arrays.asList(
				new Record(168724, "NL22ABNA0857467069", 18.52, +40.09, "Assigment for Alireza", 58.61),
				new Record(168724, "NL22ABNA0857467069", 63.72, -39.74, "Assigment for Alireza", 23.98));
		List<List<Record>> duplicateRecords = validatorService.getDuplicateRecords(inputList);
		assertEquals(inputList.size(), duplicateRecords.get(0).size());

	}
	
	/**
	 * 
	 * scenario : Duplicate check in given statements
	 * 
	 */
	@Test
	public void getDuplicateRecordsWithCorrectValuesTest() {
		List<Record> inputList = Arrays.asList(
				new Record(172823, "NL22ABNA0857467069", 18.52, +40.09, "Assigment for Alireza", 58.61),
				new Record(172833, "NL22ABNA0857467069", 63.72, -39.74, "Assigment for Alireza", 23.98));
		List<List<Record>> duplicateRecords = validatorService.getDuplicateRecords(inputList);
		assertEquals(0, duplicateRecords.size());

	}
	
	/**
	 *  
	 * scenario : EndBalance validation in given statement
	 * 
	 */
	@Test
	public void getEndBalanceRecordsWithCorrectValueTest() {
		List<Record> inputList = Arrays.asList(
				new Record(168725, "NL69ABNA0433647324", 63.72, -39.74, "Assigment for Alireza", 23.98),
				new Record(168726, "NL43AEGO0773393871", 18.52, +40.09, "Assigment for Alireza", 58.61));
		List<Record> endBalanceErrorRecords = validatorService.getEndBalanceErrorRecords(inputList);
		assertEquals(0, endBalanceErrorRecords.size());
	}
	
	/**
	 *  
	 * scenario : EndBalance validation in given statement
	 * 
	 */
	@Test
	public void getEndBalanceErrorRecordsWithWrongValueTest() {
		List<Record> inputList = Arrays.asList(
				new Record(172833, "NL22ABNA0857467069", 77.27, -35.96, "Assigment for Alireza", -37.98),
				new Record(172833, "NL22ABNA0857467069", 17.52, +57.09, "Assigment for Alireza", -34.80));
		List<Record> endBalanceErrorRecords = validatorService.getEndBalanceErrorRecords(inputList);
		assertEquals(inputList.size(), endBalanceErrorRecords.size());

	}
	


}
