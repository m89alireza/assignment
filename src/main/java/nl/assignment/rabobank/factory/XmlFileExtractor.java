package nl.assignment.rabobank.factory;

import java.io.File;
import java.rmi.UnmarshalException;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import nl.assignment.rabobank.model.Record;
import nl.assignment.rabobank.model.Records;

/**
 * @author Ali Reza Mesgary
 *
 * 
 */

//extract the XML file to the list of Records
public class XmlFileExtractor implements Extractor {

	private File file;
	
	public XmlFileExtractor(File file) {
		super();
		this.file = file;
	}

	@Override
	public List<Record> fileExtractor() throws JAXBException, UnmarshalException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Records.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Records rootRecord = (Records) jaxbUnmarshaller.unmarshal(file);

		return rootRecord.getRecord();
	}

}
