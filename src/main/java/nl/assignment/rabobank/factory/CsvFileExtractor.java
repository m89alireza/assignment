package nl.assignment.rabobank.factory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;

import nl.assignment.rabobank.model.Record;

/**
 * @author Ali Reza Mesgary
 *
 * 
 */

/**
 * extract the CSV file to the list of Records
 *
 * 
 */

public class CsvFileExtractor implements Extractor {
	
	private File file;
	

	public CsvFileExtractor(File file) {
		super();
		this.file = file;
	}

	private static final Map<String, String> recordMapping = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;

		{
			put("Reference", "transactionRef");
			put("AccountNumber", "accountNumber");
			put("Description", "description");
			put("Start Balance", "startBalance");
			put("Mutation", "mutation");
			put("End Balance", "endBalance");
		}
	};
	private static final HeaderColumnNameTranslateMappingStrategy<Record> strategy = new HeaderColumnNameTranslateMappingStrategy<Record>();

	@Override
	public List<Record> fileExtractor() throws IllegalStateException, FileNotFoundException {

		strategy.setType(Record.class);
		strategy.setColumnMapping(recordMapping);

		CsvToBean<Record> csvToBeanBuilder = new CsvToBeanBuilder<Record>(new FileReader(file)).withType(Record.class)
				.withMappingStrategy(strategy).build();

		return csvToBeanBuilder.parse();
	}

}
