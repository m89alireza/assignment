package nl.assignment.rabobank.factory;

import java.io.File;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nl.assignment.rabobank.model.Record;
import nl.assignment.rabobank.service.ValidatorService;

/**
 * @author Ali Reza Mesgary
 *
 * 
 */
@Component
public class Executor {
	
	private final Logger logger = Logger.getLogger(this.getClass().getName());  
	private FileExtractorFactory extractorFactory;
	private ValidatorService validatorSrv;
	private String filePath = System.getProperty("filePath");
	private String currentPath = new java.io.File( "." ).getCanonicalPath();
	private final FileHandler fh = new FileHandler(currentPath.concat("\\result.log")); 

	@Autowired
	public Executor(FileExtractorFactory extractorFactory, ValidatorService validatorSrv) throws Exception {
		this.extractorFactory = extractorFactory;
		this.validatorSrv = validatorSrv;
	}

	@PostConstruct
	public void execute() throws Exception {
		
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);  

		 try {
			
        	Extractor fileExtractor = extractorFactory.extractor(new File(filePath));
        	List<Record> records = fileExtractor.fileExtractor();
        	
        	logger.warning(Messages.TRANSACTIONREF_VALIDATION_ERROR.getMessages());
        	logger.info( validatorSrv.getDuplicateRecords(records).toString());
        	
        	logger.warning(Messages.BALANCE_VALIDATION_ERROR.getMessages());
        	logger.info(validatorSrv.getEndBalanceErrorRecords(records).toString());
        	
		} catch (FileNotFoundException e) {
			logger.severe(Messages.INVALID_INPUT.getMessages());
			throw new Exception(Messages.INVALID_INPUT.getMessages());
		} catch (Exception e) {
			logger.severe(Messages.UNEXPECTED_SERVER_ERROR.getMessages());
			throw new Exception(Messages.UNEXPECTED_SERVER_ERROR.getMessages());
		}
	}

}
