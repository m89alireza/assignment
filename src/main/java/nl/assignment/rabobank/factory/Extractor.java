package nl.assignment.rabobank.factory;

import java.util.List;

import nl.assignment.rabobank.model.Record;

/**
 * @author Ali Reza Mesgary
 *
 * 
 */
public interface Extractor {

	List<Record> fileExtractor() throws Exception;
}
