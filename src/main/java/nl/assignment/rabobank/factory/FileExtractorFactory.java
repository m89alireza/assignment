package nl.assignment.rabobank.factory;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

import nl.assignment.rabobank.enums.FileType;

/**
 * @author Provide
 *
 * 
 */

// Class to find that whether this file is CSV or XML to process
@Component
public class FileExtractorFactory {

	public Extractor extractor(File file) throws Exception {

		if (FilenameUtils.isExtension(file.getName(), FileType.FILE_TYPE_CSV.getFileType())) {
			return new CsvFileExtractor(file);
		} else if (FilenameUtils.isExtension(file.getName(), FileType.FILE_TYPE_XML.getFileType())) {
			return new XmlFileExtractor(file);

		}
		throw new FileNotFoundException();
	}
}
