package nl.assignment.rabobank.enums;

/**
 * @author Provide
 *
 * 
 */
public enum Messages {

	UNEXPECTED_SERVER_ERROR("Unexpected server error"), VALIDATION_SUCCESS("Validation was successful"),
	TRANSACTIONREF_VALIDATION_ERROR("your TrasactionRefrence is duplicated"),
	BALANCE_VALIDATION_ERROR("your endbalance is incorrect"),
	INVALID_INPUT("Invalid Input"), UNSUPORTED_FILE_FORMAT("Only CSV or XML file is allowed");

	private String messages;

	Messages(String messages) {
		this.messages = messages;
	}

	public String getMessages() {
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}

}
