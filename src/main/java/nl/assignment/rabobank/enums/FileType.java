package nl.assignment.rabobank.enums;

/**
 * @author Provide
 *
 * 
 */
public enum FileType {

	FILE_TYPE_XML("xml"), FILE_TYPE_CSV("csv");

	private String fileType;

	FileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
