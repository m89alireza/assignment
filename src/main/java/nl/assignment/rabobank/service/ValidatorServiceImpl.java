package nl.assignment.rabobank.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import nl.assignment.rabobank.model.Record;

/**
 * @author Ali Reza Mesgary
 *
 * 
 */
@Service
public class ValidatorServiceImpl implements ValidatorService {

	/**
	 * @return List<Records> to get duplicate records form given list.
	 */
	public List<List<Record>> getDuplicateRecords(List<Record> statementList) {
		Map<Integer, List<Record>> duplicateMap = statementList.
                stream().
                collect(Collectors.groupingBy(Record::getTransactionRef));
		List<List<Record>> records= duplicateMap.entrySet().stream()
				.filter(e -> e.getValue().size() > 1)
				.map(t -> t.getValue())
				.collect(Collectors.toList());
		return records;
	}

	/**
	 * @return List<Records> trye to find if start balanced plus mutation is equal
	 *         to endbalance or not
	 */
	public List<Record> getEndBalanceErrorRecords(List<Record> records) {
		List<Record> endBalanceErrorRecords = new ArrayList<Record>();
		for (Record record : records) {
			DecimalFormat df = new DecimalFormat("#.##");

			if (Double
					.valueOf(df.format((Double.valueOf(df.format(record.getStartBalance()))
							+ Double.valueOf(df.format(record.getMutation())))))
					- Double.valueOf(df.format(record.getEndBalance())) != 0.0) {
				endBalanceErrorRecords.add(record);
			}
		}
		return endBalanceErrorRecords;
	}
}
