package nl.assignment.rabobank.service;

import java.util.List;

import nl.assignment.rabobank.model.Record;

/**
 * @author Ali Reza Mesgary
 *
 * 
 */
public interface ValidatorService {

	public List<List<Record>> getDuplicateRecords(List<Record> records);

	public List<Record> getEndBalanceErrorRecords(List<Record> records);
}
