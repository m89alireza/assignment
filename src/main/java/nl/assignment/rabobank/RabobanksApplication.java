package nl.assignment.rabobank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabobanksApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabobanksApplication.class, args);
	}

}
